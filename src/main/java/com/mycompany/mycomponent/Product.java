/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author Golf
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "espresso",40,"image1.jpg"));
        list.add(new Product(2, "espresso2",50,"image1.jpg"));
        list.add(new Product(3, "espresso3",40,"image1.jpg"));
        list.add(new Product(4, "americano",30,"image2.jpg"));
        list.add(new Product(5, "americano2",30,"image2.jpg"));
        list.add(new Product(6, "americano3",40,"image2.jpg"));
        list.add(new Product(7, "americano4",40,"image2.jpg"));
        list.add(new Product(8, "green tea",50,"image3.jpg"));
        list.add(new Product(9, "green tea2",40,"image3.jpg"));
        return list;
    }
}
